package ru.itcube64;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;

public class Bot extends TelegramLongPollingBot {
    private String botName = "itcube64_test_bot";
    private String botToken = "6309523076:AAGQKeh9pV4dFCZnLXJIMiqpt7U13Y78m84";
    private Storage storage;
    private ReplyKeyboardMarkup replyKeyboardMarkup;

    public Bot() {
        storage = new Storage();
        initKeyboard();
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage() && update.getMessage().hasText()) {
                Message inMess = update.getMessage();
                String chatId = inMess.getChatId().toString();
                String response = parseMess(inMess.getText());

                SendMessage outMess = new SendMessage();
                outMess.setChatId(chatId);
                outMess.setText(response);
                outMess.setReplyMarkup(replyKeyboardMarkup);

                execute(outMess);
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private String parseMess(String textMess) {
        String response;

        if (textMess.equals("/start")) {
            response = "Приветствую, бот знает много цитат. Жми /get, чтобы " +
                    "получить случайную из них.";
        } else if (textMess.equals("/get") || textMess.equals("Просвяти")) {
            response = storage.getRandQuote();
        } else {
            response = "Сообщение не распознано!";
        }

        return response;
    }

    private void initKeyboard() {
        replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        ArrayList<KeyboardRow> keyboardRows = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRows.add(keyboardRow);
        keyboardRow.add(new KeyboardButton("Просвяти"));
        replyKeyboardMarkup.setKeyboard(keyboardRows);
    }
}
