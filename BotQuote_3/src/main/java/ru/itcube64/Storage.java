package ru.itcube64;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class Storage {
    private ArrayList<String> quoteList;

    public Storage() {
        quoteList = new ArrayList<>();

        parser("https://citatnica.ru/citaty/mudrye-tsitaty-velikih-lyudej");

//        quoteList.add("Начинать всегда стоит с того, что сеет сомнения. \n\nБорис Стругацкий");
//        quoteList.add("80% успеха - это появится в нужном месте в нужное время. \n\nВуди Аллен");
//        quoteList.add("Мы должны признать очевидное: понимают лишь те, кто хочет понять. \n\nБернар Вербер");
    }

    public String getRandQuote() {
        int randValue = (int) (Math.random() * quoteList.size());

        return quoteList.get(randValue);
    }

    private void parser(String strURL) {
        String className = "su-note-inner su-u-clearfix su-u-trim";
        Document document = null;

        try {
            document = Jsoup.connect(strURL).maxBodySize(0).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elQuote = document.getElementsByClass(className);
        elQuote.forEach(el -> {
            quoteList.add(el.text());
        });
    }
}
